// syndicate no-op pluggable transport server. Works only as a managed proxy.
//
// Usage (in torrc):
//
//	BridgeRelay 1
//	ORPort 9001
//	ExtORPort 6669
//	ServerTransportPlugin syndicate exec dummy-server
package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/syncthing/syncthing/lib/relay/client"
	"github.com/syncthing/syncthing/lib/relay/protocol"
	"gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/goptlib"
)

var ptInfo pt.ServerInfo

func copyLoop(a, b net.Conn) {
	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		io.Copy(b, a)
		wg.Done()
	}()
	go func() {
		io.Copy(a, b)
		wg.Done()
	}()

	wg.Wait()
}

func handler(conn net.Conn) error {
	defer conn.Close()

	or, err := pt.DialOr(&ptInfo, conn.RemoteAddr().String(), "syndicate")
	if err != nil {
		return err
	}
	defer or.Close()

	copyLoop(conn, or)

	return nil
}

func acceptLoop(ctx context.Context, ln *client.RelayClient) error {
	inviteRecv := make(chan protocol.SessionInvitation)
	go func() {
		for invite := range (*ln).Invitations() {
			select {
			case inviteRecv <- invite:
			default:
			}
		}
	}()
	for {
		invite := <-inviteRecv
		conn, err := client.JoinSession(ctx, invite)
		if err != nil {
			return err
		}
		go handler(conn)
	}
}

func main() {
	var err error
	var cert tls.Certificate

	ptInfo, err = pt.ServerSetup(nil)
	if err != nil {
		os.Exit(1)
	}

	pt.ReportVersion("syndicate-server", "0.1")

	cancellers := make([]context.CancelFunc, 0)
	for _, bindaddr := range ptInfo.Bindaddrs {
		switch bindaddr.MethodName {
		case "relay":
			// Rather than listening on local addresses, we connect to syncthing relays
			// ln, err := net.ListenTCP("tcp", bindaddr.Addr)
			// if err != nil {
			// 	pt.SmethodError(bindaddr.MethodName, err.Error())
			// 	break
			// }
			ctx, cancel := context.WithCancel(context.Background())
			id, ok := bindaddr.Options.Get("id")
			if !ok {
				pt.SmethodError(bindaddr.MethodName, "invalid relay address")
				continue
			}
			addressString := fmt.Sprintf("relay://%s/?id=%s", bindaddr.Addr.String(), id)
			relayURL, _ := url.Parse(addressString)
			relay, err := client.NewClient(relayURL, []tls.Certificate{cert}, time.Second*10)
			if err != nil {
				pt.ProxyError("Failed to connect to relay")
				continue
			}
			go relay.Serve(ctx)
			go acceptLoop(ctx, &relay)
			pt.Smethod(bindaddr.MethodName, bindaddr.Addr)
			cancellers = append(cancellers, cancel)
		default:
			pt.SmethodError(bindaddr.MethodName, "no such method")
		}
	}
	pt.SmethodsDone()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM)

	if os.Getenv("TOR_PT_EXIT_ON_STDIN_CLOSE") == "1" {
		// This environment variable means we should treat EOF on stdin
		// just like SIGTERM: https://bugs.torproject.org/15435.
		go func() {
			io.Copy(io.Discard, os.Stdin)
			sigChan <- syscall.SIGTERM
		}()
	}

	// wait for a signal
	<-sigChan

	// signal received, shut down
	for _, cancel := range cancellers {
		cancel()
	}
}
